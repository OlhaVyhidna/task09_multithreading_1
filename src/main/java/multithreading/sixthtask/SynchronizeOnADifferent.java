package multithreading.sixthtask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeOnADifferent {

  private static Logger LOG = LogManager.getLogger(SynchronizeOnADifferent.class);
  private final Object sync1 = new Object();
  private final Object sync2 = new Object();
  private final Object sync3 = new Object();

  public void methodFirst() {
    synchronized (sync1) {
      for (int i = 0; i < 200; i++) {
        LOG.info("Synchronize on the sync1 method 1 ");
      }
    }
  }

  public void methodSecond() {
    synchronized (sync2) {
      for (int i = 0; i < 200; i++) {
        LOG.info("Synchronize on the sync2 method 2 ");
      }
    }
  }

  public void thirdMethod() {
    synchronized (sync3) {
      for (int i = 0; i < 200; i++) {
        LOG.info("Synchronize on the sync3 method 3 ");
      }
    }
  }

}
