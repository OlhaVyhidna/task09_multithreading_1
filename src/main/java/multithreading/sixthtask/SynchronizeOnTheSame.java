package multithreading.sixthtask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeOnTheSame {

  private static Logger LOG = LogManager.getLogger(SynchronizeOnTheSame.class);
  private final Object sync = new Object();

  public void methodFirst() {
    synchronized (sync) {
      for (int i = 0; i < 200; i++) {
        LOG.info("Synchronize on the same method 1 value");
      }
    }
  }

  public void methodSecond() {
    synchronized (sync) {
      for (int i = 0; i < 200; i++) {
        LOG.info("Synchronize on the same method 2 value");
      }
    }
  }

  public void thirdMethod() {
    synchronized (sync) {
      for (int i = 0; i < 200; i++) {
        LOG.info("Synchronize on the same method 3 value");
      }
    }
  }


}
