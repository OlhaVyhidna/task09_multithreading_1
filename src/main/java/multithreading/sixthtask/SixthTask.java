package multithreading.sixthtask;

public class SixthTask {

  public static void main(String[] args) throws InterruptedException {
    SynchronizeOnTheSame synchronizeOnTheSame = new SynchronizeOnTheSame();
    SynchronizeOnADifferent synchronizeOnADifferent = new SynchronizeOnADifferent();
    Thread thread1= new Thread(synchronizeOnTheSame::methodFirst);
    Thread thread2 = new Thread(synchronizeOnTheSame::methodSecond);
    Thread thread3 = new Thread(synchronizeOnTheSame::thirdMethod);
    thread1.start();
    thread2.start();
    thread3.start();
    thread1.join();
    thread2.join();
    thread3.join();

    Thread dSynchron1 = new Thread(synchronizeOnADifferent::methodFirst);
    Thread dSynchron2 = new Thread(synchronizeOnADifferent::methodSecond);
    Thread dSynchron3 = new Thread(synchronizeOnADifferent::thirdMethod);
    dSynchron1.start();
    dSynchron2.start();
    dSynchron3.start();



  }


}
