package multithreading.pingpong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
  private static Logger LOG = LogManager.getLogger(PingPong.class);
  private static final Object sync = new Object();

  public void game() throws InterruptedException {
    Thread ping = new Thread(new Runnable() {
      String message = "ping";
      @Override
      public void run() {
        synchronized (sync) {
          while (true) {
            LOG.info(message);
          sync.notify();
            try {
              sync.wait();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
    });

    Thread pong = new Thread(new Runnable() {
      String message = "pong";
      @Override
      public void run() {
        synchronized (sync) {
          while (true) {
            LOG.info(message);
            sync.notify();
            try {
              sync.wait();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
    });
    ping.setDaemon(true);
    pong.setDaemon(true);
    ping.start();
    pong.start();
    Thread.sleep(2000);

  }

}
