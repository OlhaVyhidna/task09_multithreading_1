package multithreading.fifthtask;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import multithreading.fibonacci.FibonacciThirdTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FifthTask {
  private static Logger LOG = LogManager.getLogger(FibonacciThirdTask.class);
  int numberOfObjects;

  public FifthTask(int numberOfObjects) {
    this.numberOfObjects = numberOfObjects;
  }

  public void startTask() {

    for (int i = 0; i < numberOfObjects; i++) {
    ScheduledExecutorService scheduledExecutorService = Executors
        .newScheduledThreadPool(2);
      Runnable task = () -> {
        System.out.println("Some actions");
      };
      long random = new Random().nextInt(10) + 1;
      ScheduledFuture<?> schedule = scheduledExecutorService
          .schedule(task, random, TimeUnit.SECONDS);
      final long delay = schedule.getDelay(TimeUnit.SECONDS);
      System.out.println("delay " + delay);
      scheduledExecutorService.shutdown();
    }
  }

}
