package multithreading.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciFirstTask {
  private static Logger LOG = LogManager.getLogger(FibonacciSequence.class);
  private FibonacciSequence fibonacciSequence;

  public void startTask() throws InterruptedException {
    Thread thread1 = new Thread(new Runnable() {
      @Override
      public void run() {
        fibonacciSequence = new FibonacciSequence(8);
        final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
        LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
        for (int i : fibonacciSequence1) {
          LOG.info(i);
        }
      }
    });

    Thread thread2 = new Thread(new Runnable() {
      @Override
      public void run() {
        fibonacciSequence = new FibonacciSequence(5);
        final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
        LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
        for (int i : fibonacciSequence1) {
          LOG.info(i);
        }
      }
    });


    Thread thread3 = new Thread(new Runnable() {
      @Override
      public void run() {
        fibonacciSequence = new FibonacciSequence(12);
        final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
        LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
        for (int i : fibonacciSequence1) {
          LOG.info(i);
        }
      }
    });

    thread1.start();
    thread1.join();
    thread2.start();
    thread2.join();
    thread3.start();
  }

}
