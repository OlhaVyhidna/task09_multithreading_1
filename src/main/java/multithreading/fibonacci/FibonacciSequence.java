package multithreading.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciSequence {

  private static Logger LOG = LogManager.getLogger(FibonacciSequence.class);
  private int[] fibonacciSequence;
  private int fibonacciNumber;

  public FibonacciSequence(Integer fibonacciNumber) {
    this.fibonacciNumber = fibonacciNumber;
    fillTheSequence(fibonacciNumber);
  }

  private void fillTheSequence(int fibonacciNumber) {
    if (fibonacciNumber <= 1) {
      LOG.error("number is too small");
      fibonacciSequence = new int[1];
      return;
    } else {
      fibonacciSequence = new int[fibonacciNumber];
      fibonacciSequence[0] = 1;
      fibonacciSequence[1] = 1;
      for (int i = 2; i<fibonacciNumber; i++){
        fibonacciSequence[i] = fibonacciSequence[i-1]+fibonacciSequence[i-2];
      }
    }
  }

  public int[] getFibonacciSequence() {
    return fibonacciSequence;
  }

  public void setFibonacciSequence(int[] fibonacciSequence) {
    this.fibonacciSequence = fibonacciSequence;
  }

  public int getFibonacciNumber() {
    return fibonacciNumber;
  }

  public void setFibonacciNumber(int fibonacciNumber) {
    this.fibonacciNumber = fibonacciNumber;
  }
}
