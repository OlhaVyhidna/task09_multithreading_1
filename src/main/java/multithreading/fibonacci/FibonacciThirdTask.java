package multithreading.fibonacci;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciThirdTask {
  private static Logger LOG = LogManager.getLogger(FibonacciThirdTask.class);

  public void startTask() throws InterruptedException {
    ExecutorService executorService = Executors.newWorkStealingPool();
    List<Callable<Integer>> callables = Arrays.asList(()->5,()->8);
    int sum = executorService.invokeAll(callables)
        .stream().mapToInt(integerFuture -> {
          int[] sec = new int[1];
      try {
      sec = new FibonacciSequence((integerFuture.get())).getFibonacciSequence();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
      return Arrays.stream(sec).sum();
    }).sum();

    LOG.info("sum of sequences of 5 and 8  is " + sum);

  }

}
