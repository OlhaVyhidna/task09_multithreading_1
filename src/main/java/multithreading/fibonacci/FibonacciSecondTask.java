package multithreading.fibonacci;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciSecondTask {

  private static Logger LOG = LogManager.getLogger(FibonacciSequence.class);
  private FibonacciSequence fibonacciSequence;

  public void startTask() {
    ExecutorService executorService = Executors.newSingleThreadExecutor();
    executorService.submit(() -> {
      fibonacciSequence = new FibonacciSequence(7);
      final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
      LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
      for (int i : fibonacciSequence1) {
        LOG.info(i);
      }
    });

    executorService.shutdown();

    ExecutorService executorService1 = Executors.newCachedThreadPool();
    executorService1.execute(() -> {
      fibonacciSequence = new FibonacciSequence(4);
      final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
      LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
      for (int i : fibonacciSequence1) {
        LOG.info(i);
      }
    });
    executorService1.execute(() -> {
      fibonacciSequence = new FibonacciSequence(5);
      final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
      LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
      for (int i : fibonacciSequence1) {
        LOG.info(i);
      }
    });

    executorService1.shutdown();

    ExecutorService executorService2 = Executors.newFixedThreadPool(2);
    executorService2.execute(() -> {
      fibonacciSequence = new FibonacciSequence(6);
      final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
      LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
      for (int i : fibonacciSequence1) {
        LOG.info(i);
      }
    });
    executorService2.execute(() -> {
      fibonacciSequence = new FibonacciSequence(8);
      final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
      LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
      for (int i : fibonacciSequence1) {
        LOG.info(i);
      }
    });
    executorService2.execute(() -> {
      fibonacciSequence = new FibonacciSequence(11);
      final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
      LOG.info("Fibonacci sequence for " + fibonacciSequence.getFibonacciNumber());
      for (int i : fibonacciSequence1) {
        LOG.info(i);
      }
    });
    executorService2.shutdown();
  }


}
