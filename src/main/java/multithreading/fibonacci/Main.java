package multithreading.fibonacci;

import multithreading.fifthtask.FifthTask;

public class Main {

  public static void main(String[] args) throws InterruptedException {
//    FibonacciSequence fibonacciSequence = new FibonacciSequence(2);
//    final int[] fibonacciSequence1 = fibonacciSequence.getFibonacciSequence();
//    for (int i : fibonacciSequence1) {
//      System.out.println(i);
//    }

//    FibonacciFirstTask firstTask = new FibonacciFirstTask();
//    firstTask.startTask();

//    FibonacciSecondTask fibonacciSecondTask = new FibonacciSecondTask();
//    fibonacciSecondTask.startTask();

//    FibonacciThirdTask thirdTask = new FibonacciThirdTask();
//    thirdTask.startTask();

    FifthTask fifthTask = new FifthTask(3);
    fifthTask.startTask();
  }

}
